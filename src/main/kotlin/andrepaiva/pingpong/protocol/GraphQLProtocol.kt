package andrepaiva.pingpong.protocol

object GraphQLProtocol {

    @NoArg data class Request(val query: String, val operationName: String?, val variables: Map<String, Any?>?)
    @NoArg data class Response(val data: Any? = null, val errors: Collection<ErrorMessage>? = null)
    @NoArg data class ErrorMessage(var key: String? = null, var message: String? = null)

}
