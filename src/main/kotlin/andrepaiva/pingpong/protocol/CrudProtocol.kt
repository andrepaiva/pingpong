package andrepaiva.pingpong.protocol

import java.io.Serializable

object CrudProtocol {

    @NoArg data class UUID(val uuid: String) : Serializable
    @NoArg data class Success(val success: Boolean = true) : Serializable

    @NoArg data class ValidationException(val errors: Set<String>) : RuntimeException() {
        constructor(vararg errors: String) : this(setOf(*errors))
    }

}
