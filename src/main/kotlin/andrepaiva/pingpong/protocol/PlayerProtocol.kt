package andrepaiva.pingpong.protocol

object PlayerProtocol {

    @NoArg data class CreatePlayer(val name: String)

}
