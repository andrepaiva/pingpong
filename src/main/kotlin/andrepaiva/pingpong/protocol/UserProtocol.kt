package andrepaiva.pingpong.protocol

import andrepaiva.pingpong.model.User
import java.time.LocalDateTime

object UserProtocol {

    @NoArg data class SignUp(val name: String, val email: String, val password: String)
    @NoArg data class Login(val email: String, val password: String)
    @NoArg data class Logged(
        var user: User,
        val accessToken: String,
        val startedAt: LocalDateTime,
        val expiration: Long
    )

}
