package andrepaiva.pingpong.protocol

object EmailProtocol {

    @NoArg data class Welcome(val name: String?, val to: String)
    @NoArg data class Result(val name: String?, val to: String, val winner: String, val loser: String)

}
