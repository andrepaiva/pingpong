package andrepaiva.pingpong.protocol

import andrepaiva.pingpong.protocol.CrudProtocol.UUID

object GameProtocol {

    @NoArg data class CreateGame(
        val score1: CreateScore,
        val score2: CreateScore
    )
    @NoArg data class CreateScore(
        val player: UUID,
        val score: Int
    )

}
