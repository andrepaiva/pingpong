package andrepaiva.pingpong.repository

import andrepaiva.pingpong.model.User

class UserRepository : EntityRepository<User>() {

    fun findByEmail(email: String?): User? = findByProp("email", email)
    fun existsByEmail(email: String?): Boolean = findByEmail(email) != null

}
