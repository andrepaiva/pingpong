package andrepaiva.pingpong.repository

import andrepaiva.pingpong.app
import andrepaiva.pingpong.model.Game

class GameRepository : EntityRepository<Game>() {

    private val scoreRepository: ScoreRepository = app.get()
    private val playerRepository: PlayerRepository = app.get()

    override fun preCreate(entity: Game): Game = super.preCreate(entity).apply {
        score1 = scoreRepository.create(score1!!)
        score2 = scoreRepository.create(score2!!)
        if (score1!!.score!! > score2!!.score!!) {
            winner = playerRepository.updateWinner(score1!!.player!!)
            loser = playerRepository.updateLoser(score2!!.player!!)
        } else {
            winner = playerRepository.updateWinner(score2!!.player!!)
            loser = playerRepository.updateLoser(score1!!.player!!)
        }
    }

}
