package andrepaiva.pingpong.repository

import andrepaiva.pingpong.model.Player

class PlayerRepository : EntityRepository<Player>() {

    override fun create(entity: Player): Player {
        return findByName(entity.name) ?: super.create(entity)
    }

    fun findByName(name: String?): Player? = findByProp("name", name)

    fun updateWinner(player: Player): Player = update(player.apply { wins = wins!! + 1 })
    fun updateLoser(player: Player): Player = update(player.apply { losses = losses!! + 1 })

}
