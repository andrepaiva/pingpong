package andrepaiva.pingpong.repository

import andrepaiva.pingpong.app
import andrepaiva.pingpong.model.Score

class ScoreRepository : EntityRepository<Score>() {

    private val playerRepository: PlayerRepository = app.get()

    override fun preCreate(entity: Score): Score = super.preCreate(entity).apply {
        player = player?.let { playerRepository.findByUuid(it) }
    }

}
