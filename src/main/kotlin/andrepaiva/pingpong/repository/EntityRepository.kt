package andrepaiva.pingpong.repository

import andrepaiva.pingpong.app
import andrepaiva.pingpong.model.Entity
import andrepaiva.pingpong.util.genericClass
import org.neo4j.ogm.cypher.ComparisonOperator.EQUALS
import org.neo4j.ogm.cypher.Filter
import org.neo4j.ogm.cypher.Filters
import org.neo4j.ogm.session.Session
import kotlin.reflect.KClass

abstract class EntityRepository<T : Entity> {

    protected val entityClass: KClass<T> = genericClass()
    protected val entityName: String = entityClass.simpleName!!

    val session: Session get() = app.get()

    protected open fun preCreate(entity: T): T = entity.apply { initUuid() }
    protected open fun postCreate(entity: T): T = entity.also { session.clear() }
    open fun create(entity: T): T = run {
        preCreate(entity)
        save(entity)
        postCreate(entity)
    }

    protected open fun preUpdate(entity: T): T = entity
    protected open fun postUpdate(entity: T): T = entity.also { session.clear() }
    open fun update(entity: T): T = run {
        preUpdate(entity)
        save(entity)
        postUpdate(entity)
    }

    protected open fun preRemove(uuid: String?): T? = findByUuid(uuid)
    protected open fun postRemove(dbEntity: T): T = dbEntity.also { session.clear() }

    open fun remove(entity: T): T? = remove(entity.uuid)
    open fun remove(uuid: String?): T? = preRemove(uuid)?.let {
        delete(it)
        postRemove(it)
    }

    open fun findByUuid(entity: T): T? = findByUuid(entity.uuid)
    open fun findByUuid(uuid: String?): T? = findByProp("uuid", uuid)
    open fun findByProp(name: String, value: Any?): T? = findAllByProp(name, value)?.firstOrNull()
    open fun findAllByProp(name: String, value: Any?): MutableCollection<T>? =
        value?.let { findAll(Filters(Filter(name, EQUALS, value))) }

    open fun existsByUuid(entity: T): Boolean = existsByUuid(entity.uuid)
    open fun existsByUuid(uuid: String?): Boolean = findByUuid(uuid) != null

    open fun findAll(filters: Filters): MutableCollection<T>? = session.loadAll(entityClass.java, filters)
    open fun findAll(depth: Int = 3): MutableCollection<T>? = session.loadAll(entityClass.java, depth)

    open fun query(query: String): Iterable<T> = session.query(entityClass.java, query, mapOf<String, Any>())!!

    open fun save(entity: T): T = session.save(entity, 1).let { entity }

    open fun delete(entity: T) = session.delete(entity)

}
