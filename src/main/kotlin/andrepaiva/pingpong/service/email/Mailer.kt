package andrepaiva.pingpong.service.email

import andrepaiva.pingpong.app
import com.typesafe.config.Config
import org.apache.commons.mail.HtmlEmail
import org.apache.velocity.Template
import org.apache.velocity.VelocityContext
import org.apache.velocity.app.VelocityEngine
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader
import java.io.StringWriter
import java.util.*

abstract class Mailer(fileName: String) {

    protected val config: Config = app.config
    protected val uri: String = config.getString("web.uri")
    protected val ssl: Boolean = config.getBoolean("service.email.ssl")
    protected val port: Int = config.getInt("service.email.port")
    protected val host: String = config.getString("service.email.host")
    protected val user: String = config.getString("service.email.user")
    protected val pass: String = config.getString("service.email.pass")
    protected val from: String = config.getString("service.email.from")
    protected val name: String = config.getString("service.email.name")
    protected val template: Template = initTemplate(fileName)

    protected open fun initTemplate(fileName: String): Template =
        VelocityEngine().apply { init(buildProps()) }.getTemplate("email/$fileName.html", Charsets.UTF_8.name())

    protected open fun buildProps(): Properties = Properties().apply {
        setProperty("resource.loader", "file")
        setProperty("file.resource.loader.class", ClasspathResourceLoader::class.java.name)
    }

    protected open fun create(sub: String, to: String, msg: String): HtmlEmail = HtmlEmail().apply {
        hostName = host
        setSmtpPort(port)
        setAuthentication(user, pass)
        isSSLOnConnect = ssl
        isSSLCheckServerIdentity = ssl
        subject = sub
        setFrom(from, name)
        addTo(to)
        setHtmlMsg(msg)
    }

    protected open fun email(sub: String, to: String, msg: String): String = create(sub, to, msg).send()

    protected open fun build(context: VelocityContext): String =
        StringWriter().also { template.merge(context, it) }.toString()

    // FIXME - I18N
    protected open fun getName(name: String?): String = when (name) {
        null -> "there"
        else -> name
    }

}
