package andrepaiva.pingpong.service.email

import andrepaiva.pingpong.protocol.EmailProtocol
import org.apache.velocity.VelocityContext

class ResultMailer : Mailer("result") {

    fun result(data: EmailProtocol.Result) = try {
        email("Ping-pong Game Result!", data.to, buildResult(data.name, data.winner, data.loser))
    } catch (e: Exception) {
        //TODO Log error
    }

    private fun buildResult(name: String?, winner: String, loser: String): String = VelocityContext().let {
        it.put("NAME", getName(name))
        it.put("WINNER", winner)
        it.put("LOSER", loser)
        it.put("URI", uri)
        build(it)
    }

}
