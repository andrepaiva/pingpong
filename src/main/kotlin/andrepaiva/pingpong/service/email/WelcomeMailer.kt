package andrepaiva.pingpong.service.email

import andrepaiva.pingpong.protocol.EmailProtocol.Welcome
import org.apache.velocity.VelocityContext

class WelcomeMailer : Mailer("welcome") {

    fun welcome(data: Welcome) = try {
        email("Welcome to Ping-pong Game!", data.to, buildWelcome(data.name))
    } catch (e: Exception) {
        //TODO Log error
    }

    private fun buildWelcome(name: String?): String = VelocityContext().let {
        it.put("NAME", getName(name))
        it.put("URI", uri)
        build(it)
    }

}
