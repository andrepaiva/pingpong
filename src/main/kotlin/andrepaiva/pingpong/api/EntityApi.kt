package andrepaiva.pingpong.api

import andrepaiva.pingpong.app
import andrepaiva.pingpong.cache.LoginSession
import andrepaiva.pingpong.model.Entity
import andrepaiva.pingpong.protocol.CrudProtocol.Success
import andrepaiva.pingpong.protocol.CrudProtocol.ValidationException
import andrepaiva.pingpong.protocol.UserProtocol.Logged
import andrepaiva.pingpong.repository.EntityRepository
import andrepaiva.pingpong.util.genericClass
import kotlin.reflect.KClass

abstract class EntityApi<T : Entity, out R : EntityRepository<T>> {

    protected val entityClass: KClass<T> = genericClass()
    protected val entityName: String = entityClass.simpleName!!.decapitalize()

    protected val repository: R = app.get(genericClass<R>(1))
    protected val loginSession: LoginSession = app.get()

    protected fun errorAuth(): Nothing = throw ValidationException("auth.not.authorized")

    open fun <D> auth(accessToken: String, block: (Logged) -> D): D =
        loginSession.get(accessToken)?.run(block) ?: errorAuth()

    protected open fun create(entity: T): T = repository.create(entity)
    protected open fun update(entity: T): T = repository.update(entity)
    protected open fun remove(uuid: String?): T? = repository.remove(uuid)
    protected open fun findByUuid(uuid: String?): T? = repository.findByUuid(uuid)
    protected open fun findAll(): MutableCollection<T>? = repository.findAll()

    open fun create(accessToken: String, entity: T): T = auth(accessToken) { create(entity) }
    open fun update(accessToken: String, entity: T): Success = auth(accessToken) { update(entity).run { Success() } }
    open fun remove(accessToken: String, uuid: String): Success = auth(accessToken) { remove(uuid).run { Success() } }
    open fun findAll(accessToken: String): MutableCollection<T>? = auth(accessToken) { findAll() }
}
