package andrepaiva.pingpong.api

import andrepaiva.pingpong.model.Player
import andrepaiva.pingpong.repository.PlayerRepository

class PlayerApi : EntityApi<Player, PlayerRepository>() {

    override fun create(entity: Player): Player = super.create(entity.apply {
        wins = 0
        losses = 0
    })
}
