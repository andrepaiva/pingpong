package andrepaiva.pingpong.api

import andrepaiva.pingpong.app
import andrepaiva.pingpong.model.User
import andrepaiva.pingpong.protocol.CrudProtocol.Success
import andrepaiva.pingpong.protocol.CrudProtocol.ValidationException
import andrepaiva.pingpong.protocol.EmailProtocol.Welcome
import andrepaiva.pingpong.protocol.UserProtocol.Logged
import andrepaiva.pingpong.protocol.UserProtocol.Login
import andrepaiva.pingpong.repository.UserRepository
import andrepaiva.pingpong.service.email.WelcomeMailer

class UserApi : EntityApi<User, UserRepository>() {

    private val welcomeMailer: WelcomeMailer = app.get()

    override fun create(entity: User): User = super.create(entity.apply { password = app.encrypt(password!!) })

    fun removeByToken(accessToken: String): Success = auth(accessToken) {
        remove(it.user.uuid)
        loginSession.remove(accessToken)
        Success()
    }

    fun findByToken(accessToken: String): User? = auth(accessToken) { findByUuid(it.user.uuid) }

    fun signUp(data: User): Logged = when (repository.existsByEmail(data.email)) {
        true -> throw ValidationException("user.email.duplicated")
        else -> create(data).let {
            welcomeMailer.welcome(Welcome(data.name, data.email!!))
            login(it)
        }
    }

    fun login(data: Login): Logged = repository.findByEmail(data.email)
        ?.takeIf { it.password == app.encrypt(data.password) }
        ?.let { login(it) }
        ?: throw ValidationException("login.not.found")

    private fun login(entity: User): Logged = loginSession.add(entity)

    fun logout(accessToken: String): Success = loginSession.remove(accessToken).run { Success() }

}
