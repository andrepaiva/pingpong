package andrepaiva.pingpong.cache

import andrepaiva.pingpong.app
import andrepaiva.pingpong.model.User
import andrepaiva.pingpong.protocol.CrudProtocol.ValidationException
import andrepaiva.pingpong.protocol.UserProtocol.Logged
import com.github.benmanes.caffeine.cache.Cache
import com.github.benmanes.caffeine.cache.Caffeine
import com.typesafe.config.Config
import java.time.LocalDateTime.now
import java.util.concurrent.TimeUnit.SECONDS

class LoginSession {

    private val config: Config = app.config
    private val size: Long = config.getLong("login.token.size")
    private val time: Long = config.getLong("login.access.time")
    private val cache: Cache<String, Logged> = Caffeine.newBuilder().maximumSize(size).expireAfterWrite(time, SECONDS).build()

    fun add(user: User): Logged = Logged(user, app.encrypt(), now(), time).also { cache.put(it.accessToken, it) }

    fun get(accessToken: String): Logged? = cache.getIfPresent(accessToken)

    fun exists(accessToken: String): Boolean = get(accessToken) != null

    fun remove(accessToken: String) =
        if (exists(accessToken)) cache.invalidate(accessToken)
        else throw ValidationException("logout.token.invalid")

}