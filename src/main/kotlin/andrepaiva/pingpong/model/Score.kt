package andrepaiva.pingpong.model

class Score(uuid: String? = null) : Entity(uuid) {

    var player: Player? = null
    var score: Int? = null

}