package andrepaiva.pingpong.model

import org.neo4j.ogm.annotation.Id
import java.io.Serializable
import java.util.UUID.randomUUID

abstract class Entity(@Id var uuid: String? = null) : Serializable {

    fun initUuid() {
        uuid = uuid ?: randomUUID().toString()
    }

    override fun equals(other: Any?): Boolean = when {
        other == null -> false
        this === other -> true
        this::class != other::class -> false
        else -> uuid?.equals((other as Entity).uuid) ?: false
    }

    override fun hashCode(): Int = uuid?.hashCode() ?: 0

}
