package andrepaiva.pingpong.model

import org.neo4j.ogm.annotation.Relationship
import org.neo4j.ogm.annotation.Relationship.INCOMING

class Player(uuid: String? = null) : Entity(uuid) {

    var name: String? = null
    var wins: Int? = null
    var losses: Int? = null

    @Relationship("WINNER", direction = INCOMING)
    var wonGames: MutableSet<Game>? = null

    @Relationship("LOSER", direction = INCOMING)
    var lostGames: MutableSet<Game>? = null


}