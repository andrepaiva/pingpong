package andrepaiva.pingpong.model

class User(uuid: String? = null) : Entity(uuid) {

    var name: String? = null
    var email: String? = null
    var password: String? = null

    constructor(email: String?, password: String?) : this() {
        this.email = email
        this.password = password
    }

}
