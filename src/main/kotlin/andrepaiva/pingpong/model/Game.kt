package andrepaiva.pingpong.model

class Game(uuid: String? = null) : Entity(uuid) {

    var winner: Player? = null
    var loser: Player? = null
    var score1: Score? = null
    var score2: Score? = null

}