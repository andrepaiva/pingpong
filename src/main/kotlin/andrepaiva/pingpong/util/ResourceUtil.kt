package andrepaiva.pingpong.util

import java.io.File
import java.net.URL
import java.net.URLDecoder.decode
import java.nio.charset.StandardCharsets.UTF_8

object ResourceUtil {

    fun getResourceUrl(name: String): URL = Thread.currentThread().contextClassLoader.getResource(name)
    fun getResourcePath(name: String): String = decode(getResourceUrl(name).path, UTF_8.name())
    fun getResourceFile(name: String): File = File(getResourcePath(name))

}
