package andrepaiva.pingpong.util

import kotlin.reflect.KClass
import kotlin.reflect.full.allSupertypes

fun <T : Any> Any.genericClass(idx: Int = 0): KClass<T> = this::class.genericClass(idx)
fun <T : Any> KClass<*>.genericClass(idx: Int = 0): KClass<T> =
    allSupertypes.find { it.arguments.isNotEmpty() }!!.let { it.arguments[idx].type!!.classifier } as KClass<T>
