package andrepaiva.pingpong.graphql

import andrepaiva.pingpong.api.GameApi
import andrepaiva.pingpong.api.PlayerApi
import andrepaiva.pingpong.api.UserApi
import andrepaiva.pingpong.app
import org.neo4j.ogm.session.Session

abstract class GraphQLApi {

    protected fun clean() = app.get<Session>().clear()
    protected val userApi: UserApi get() = run { clean(); app.get() }
    protected val playerApi: PlayerApi get() = run { clean(); app.get() }
    protected val gameApi: GameApi get() = run { clean(); app.get() }

}
