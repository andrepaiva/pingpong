package andrepaiva.pingpong.graphql

import andrepaiva.pingpong.app
import andrepaiva.pingpong.protocol.GameProtocol.CreateGame
import andrepaiva.pingpong.protocol.GameProtocol.CreateScore
import andrepaiva.pingpong.protocol.PlayerProtocol.CreatePlayer
import andrepaiva.pingpong.protocol.UserProtocol.Login
import andrepaiva.pingpong.protocol.UserProtocol.SignUp
import com.coxautodev.graphql.tools.GraphQLMutationResolver

class Mutation : GraphQLApi(), GraphQLMutationResolver {

    fun signUp(data: SignUp) = userApi.signUp(convert(data))
    fun login(data: Login) = userApi.login(data)
    fun logout(accessToken: String) = userApi.logout(accessToken)

    fun createPlayer(accessToken: String, data: CreatePlayer) = playerApi.create(accessToken, convert(data))
    fun createGame(accessToken: String, data: CreateGame) = gameApi.create(accessToken, convert(data))

    private inline fun <reified T : Any> convert(data: Any): T = app.mapper.convertValue(data, T::class.java)

}