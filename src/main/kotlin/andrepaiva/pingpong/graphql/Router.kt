package andrepaiva.pingpong.graphql

import andrepaiva.pingpong.protocol.CrudProtocol.ValidationException
import andrepaiva.pingpong.protocol.GraphQLProtocol.ErrorMessage
import andrepaiva.pingpong.protocol.GraphQLProtocol.Request
import andrepaiva.pingpong.protocol.GraphQLProtocol.Response
import andrepaiva.pingpong.util.ResourceUtil.getResourceFile
import com.coxautodev.graphql.tools.ResolverError
import com.coxautodev.graphql.tools.SchemaParser
import com.github.benmanes.caffeine.cache.Caffeine
import graphql.ExceptionWhileDataFetching
import graphql.ExecutionInput
import graphql.GraphQL
import graphql.GraphQLError
import graphql.execution.preparsed.PreparsedDocumentEntry
import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletableFuture.completedFuture

class Router {

    private val graphQL: GraphQL = initGraphQL()
    private val errorKey: String = "server.error"
    private val gqlErrorKey: String = "graphql.error"

    private fun initGraphQL(): GraphQL {
        val cache = Caffeine.newBuilder().maximumSize(1000).build<String, PreparsedDocumentEntry>()
        val schema = SchemaParser.newParser().run {
            resolvers(Query(), Mutation())
            getResourceFile("graphql").list().map { "graphql/$it" }.forEach { file(it) }
            build().makeExecutableSchema()
        }
        return GraphQL.newGraphQL(schema).preparsedDocumentProvider(cache::get).build()
    }

    fun route(req: Request): CompletableFuture<Response> = try {
        execute(ExecutionInput(req.query, req.operationName, null, null, req.variables))
    } catch (e: Throwable) {
        completedFuture(error(e))
    }

    private fun execute(input: ExecutionInput): CompletableFuture<Response> = graphQL.executeAsync(input)
        .thenApply { Response(it.getData(), errors(it.errors)) }
        .exceptionally { error(it) }

    private fun error(e: Throwable): Response = Response(null, listOf(ErrorMessage(errorKey, e.message)))
    private fun errors(errors: List<GraphQLError>): List<ErrorMessage>? = errors.flatMap {
        when (it) {
            is ExceptionWhileDataFetching -> when {
                it.exception is ResolverError -> listOf(ErrorMessage(gqlErrorKey, it.message))
                it.exception is ValidationException -> errors(it.exception as ValidationException)
                it.exception.cause is ValidationException -> errors(it.exception.cause as ValidationException)
                else -> throw it.exception
            }
            else -> listOf(ErrorMessage(gqlErrorKey, it.message))
        }
    }.takeIf { it.isNotEmpty() }

    private fun errors(exp: ValidationException): List<ErrorMessage> = exp.errors.flatMap { listOf(ErrorMessage(it)) }
}
