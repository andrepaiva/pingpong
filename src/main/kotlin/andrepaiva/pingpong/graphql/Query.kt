package andrepaiva.pingpong.graphql

import com.coxautodev.graphql.tools.GraphQLQueryResolver

class Query : GraphQLApi(), GraphQLQueryResolver {

    fun user(accessToken: String) = userApi.findByToken(accessToken)
    fun players(accessToken: String) = playerApi.findAll(accessToken)

}