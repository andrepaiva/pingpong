package andrepaiva.pingpong

import andrepaiva.pingpong.api.GameApi
import andrepaiva.pingpong.api.PlayerApi
import andrepaiva.pingpong.api.UserApi
import andrepaiva.pingpong.cache.LoginSession
import andrepaiva.pingpong.graphql.Router
import andrepaiva.pingpong.protocol.GraphQLProtocol
import andrepaiva.pingpong.repository.GameRepository
import andrepaiva.pingpong.repository.PlayerRepository
import andrepaiva.pingpong.repository.ScoreRepository
import andrepaiva.pingpong.repository.UserRepository
import andrepaiva.pingpong.service.email.WelcomeMailer
import com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature.WRITE_DATES_AS_TIMESTAMPS
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.jackson.jackson
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.async
import org.apache.commons.codec.digest.Sha2Crypt.sha256Crypt
import org.kodein.di.DKodein
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.kodein.di.internal.synchronizedIfNull
import org.kodein.di.threadLocal
import org.neo4j.ogm.config.Configuration
import org.neo4j.ogm.config.Configuration.Builder
import org.neo4j.ogm.session.Session
import org.neo4j.ogm.session.SessionFactory
import java.nio.charset.StandardCharsets.UTF_8
import java.time.Duration
import java.time.ZoneOffset.UTC
import java.util.*
import java.util.UUID.randomUUID

class Application {

    init {
        app = this
        TimeZone.setDefault(TimeZone.getTimeZone(UTC))
    }

    val config: Config = ConfigFactory.load()
    val salt: String = config.getString("app.salt")

    val mapper: ObjectMapper = jacksonObjectMapper().registerModule(JavaTimeModule())
        .disable(WRITE_DATES_AS_TIMESTAMPS).setSerializationInclusion(NON_NULL)

    val sessionFactory: SessionFactory = SessionFactory(configureDb(), "andrepaiva.pingpong.model")
    val router = Router()
    val kodein: DKodein = Kodein.direct {
        bind<Any>(UserApi::class) with singleton { UserApi() }
        bind<Any>(PlayerApi::class) with singleton { PlayerApi() }
        bind<Any>(GameApi::class) with singleton { GameApi() }
        bind<Any>(LoginSession::class) with singleton { LoginSession() }

        bind<Any>(UserRepository::class) with singleton { UserRepository() }
        bind<Any>(PlayerRepository::class) with singleton { PlayerRepository() }
        bind<Any>(ScoreRepository::class) with singleton { ScoreRepository() }
        bind<Any>(GameRepository::class) with singleton { GameRepository() }
        bind<Any>(Session::class) with singleton(threadLocal) { sessionFactory.openSession() }

        bind<Any>(WelcomeMailer::class) with singleton { WelcomeMailer() }
        //TODO Add game result mailer
    }

    fun configureDb(): Configuration = Builder().apply {
        uri(config.getString("neo4j.uri"))
        connectionPoolSize(config.getInt("neo4j.connection.pool.size"))
        encryptionLevel(config.getString("neo4j.encryption.level"))
    }.build()

    fun start() = embeddedServer(Netty, 8080) {
        install(ContentNegotiation) {
            jackson {
            }
        }

        install(DefaultHeaders)
        install(CORS) {
            method(HttpMethod.Options)
            method(HttpMethod.Get)
            method(HttpMethod.Post)
            header(HttpHeaders.AccessControlAllowHeaders)
            header(HttpHeaders.ContentType)
            header(HttpHeaders.AccessControlAllowOrigin)
            allowCredentials = true
            anyHost()
            maxAge = Duration.ofDays(1)
        }

        routing {
            post("/graphql") {
                val request = call.receive<GraphQLProtocol.Request>()
                router.route(request).thenAccept {
                    async { call.respond(it) }
                }
            }
        }
    }.start(wait = true)

    inline fun <reified T : Any> get(): T = get(T::class)
    fun <T : Any> get(tag: Any): T = kodein.instance<Any>(tag) as T

    fun encrypt(value: String = randomUUID().toString()): String =
        sha256Crypt(value.toByteArray(UTF_8), "$6$$salt").normalize()

    private fun String.normalize(): String = substring(lastIndexOf('$') + 1)

    companion object {
        var app: Application? = null
    }

}

val app = synchronizedIfNull(Application, { Application.app }, { it }, { Application() })
