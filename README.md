# Ping-pong Server

Backend service to create a ping-pong game with players and rank.

This project was implemented with GraphQL, Kotlin, Ktor, Kodein DI, Neo4j and Caffeine.

Check on [pingpong-web](https://gitlab.com/andrepaiva/pingpong-web) to run the front-end of this project.

## Running
This project uses docker-compose to facilitate the execution of the entire stack. The stack is composed of three services:
web, server and neo4j (database). You can run it using the following command:
```
# On the root folder
docker-compose up

# To run each service individually (optional):
docker-compose up -d neo4j
docker-compose up -d server
docker-compose up -d web
```

If you prefer to run the server on Intellij execute the file **Main.kt**.

### Endpoints
You can access the services on the following URLs:

- Web: http://localhost:9090

- Server GraphQL: http://localhost:8080/graphql

- Neo4j UI: http://localhost:7474

## Usage
### Login/SignUp
- To begin you'll need to sign up or login if you already have registered user.

### Rank
- After login you'll be on the Players Rank page with all wins and losses from each player.
- From there you can create a new game.

### Players
- Fill the name of two players to play.
- If you write a new name, it will create new player.
- If you write an existing name, it will retrieve the existing player.

### Playing
- Click on each player's button to score.
- The first to score eleven points wins.
- You can finish the game once there is a winner.
- At the rank page see the updated ranking results.

### Logout
- You can logout any time by clicking on the logout button.

## Features
This project has some nice features.

#### Neo4j
There is a link for Neo4j UI at the right of the top bar. Neo4j is graph database, here you'll be able to see all nodes (entities) and relationships on the database.

#### Gitlab CI and Registry
The project is configured to build docker images maintained on the Gitlab docker registry.

#### Docker-compose
As said before, this project has a docker-compose configuration which runs all the project stack on containers.

#### Welcome email
Using SendGrid API, once you sign up the service will send a welcome message to your registered email. 

#### i18n
The web project supports internationalization for EN, but can easily be improved with new languages.

### CORS
The GraphQL Server API is configured to support CORS.